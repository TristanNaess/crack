#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
int main(int argc, char ** argv)
{
    // check arg count
    if (argc != 3)
    {
        printf("hashpass requires two arguments. <file to read> <output file>\n");
        exit(1);
    }
    // create file pointers
    FILE * fp_in = fopen(argv[1], "r");
    FILE * fp_out = fopen(argv[2], "w");

    if (!fp_in || !fp_out)
    {
        printf("Error opening file\n");
        exit(2);
    }

    char password[128];
    char * hash;

    while(fgets(password, 33, fp_in))
    {
        if (password[strlen(password)-1] == '\n')
        {
            password[strlen(password)-1] = '\0';
        }
        hash = md5(password, strlen(password));
        fprintf(fp_out, "%s\n", hash);
    }
    fclose(fp_in);
    fclose(fp_out);
}
